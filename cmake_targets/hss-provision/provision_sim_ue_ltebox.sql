DELETE FROM users WHERE imsi="208930100001111";
DELETE FROM pdn WHERE users_imsi="208930100001111";
DELETE FROM pgw WHERE id="3";
DELETE FROM mmeidentity WHERE idmmeidentity="50";


INSERT INTO mmeidentity (`idmmeidentity`, `mmehost`,`mmerealm`,`UE-Reachability`) VALUES ('50', 'epc1.oaisim-redux-b.phantomnet.emulab.net', 'oaisim-redux-b.phantomnet.emulab.net','0');
INSERT INTO users (`imsi`, `msisdn`, `imei`, `imei_sv`, `ms_ps_status`, `rau_tau_timer`, `ue_ambr_ul`, `ue_ambr_dl`, `access_restriction`, `mme_cap`, `mmeidentity_idmmeidentity`, `key`, `RFSP-Index`, `urrp_mme`, `sqn`, `rand`, `OPc`) VALUES ('208930100001111', '33611111111', '35611302209414', NULL, 'PURGED', '120', '40000000', '100000000', '50', '0000000000', 50, 0x8BAF473F2F8FD09487CCCBD7097C6862 , '1', '0', 00000000000000006103, 0x65626430373737316163653836373761, 0x1006020f0a478bf6b699f15c062e42b3);
INSERT INTO pgw (`id`,`ipv4`,`ipv6`) VALUES ('3','10.0.0.2','0');
INSERT INTO pdn (`id`, `apn`, `pdn_type`, `pdn_ipv4`, `pdn_ipv6`,`aggregate_ambr_ul`, `aggregate_ambr_dl`, `pgw_id`, `users_imsi`, `qci`, `priority_level`,`pre_emp_cap`, `pre_emp_vul`, `LIPA-Permissions`) VALUES ('1000', 'oai.ipv4','IPV4', '0.0.0.0', '0:0:0:0:0:0:0:0', '50000000', '100000000', '3', '208930100001111', '9', '15', 'DISABLED','ENABLED', 'LIPA-ONLY');


SELECT * FROM users;
SELECT * FROM pdn;
